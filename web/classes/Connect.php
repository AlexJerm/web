<?php
class Connect {
	
		private $servername;
		private $username;
		private $password;
		private $dbname;
		private $conn;
			
		public function __construct() {
			
			$this->servername = "localhost";
			$this->username = "";
			$this->password = "";
			$this->dbname = "";
				
			$this->conn = mysqli_connect($this->servername, $this->username, $this->password, $this->dbname);
				
			if (!$this->conn) {
				die("Connection failed: " . mysqli_connect_error());
				return false;
			}				
			return true;
		}
		
		public function get() {
			return $this->conn;
		}
}
?>