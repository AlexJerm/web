<?php
class Auto{

	public $auto_id;
	public $model_id;
	public $reg_number;
	public $color;
	public $type;
	
	
	function add($conn) {
		$sqlString="INSERT INTO 
						autos (auto_id, model_id, reg_number, color, type)
						VALUES (NULL,'" .$this->model_id. "', '" .$this->reg_number. "', '" .$this->color. "', '" .$this->type."');";
	
		mysqli_query($conn, $sqlString);
	}
	
	function getAllAutos($conn) {
		$getAutosSqlString = "SELECT *
							FROM autos 
							JOIN models ON autos.model_id = models.model_id
							JOIN marks ON models.mark_id = marks.mark_id
							ORDER BY auto_id";
							
							
		$getAutosResult = mysqli_query($conn, $getAutosSqlString);
		return $getAutosResult;
	}
	
	function get($conn, $id) {
		$getAutosSqlString = "SELECT *
					FROM autos 
					JOIN models ON autos.model_id = models.model_id
					JOIN marks ON models.mark_id = marks.mark_id
					WHERE autos.model_id=" .$id;
		$getAutosResult = mysqli_query($conn, $getAutosSqlString);
		return $getAutosResult;
	}
	
	function getById($conn, $id) {
	
		$sql_string = "SELECT *
						FROM autos
						WHERE auto_id=".$id;
						
		$getAutosResult = mysqli_query($conn, $sql_string);

		while($getAutosRow = mysqli_fetch_array($getAutosResult))
		{	
				$this->auto_id = $getAutosRow['auto_id'];
				$this->model_id = $getAutosRow['model_id'];
				$this->reg_number = $getAutosRow['reg_number'];
				$this->color = strtolower($getAutosRow['color']);
				$this->type = $getAutosRow['type'];
		}
	}
	
	function update($conn) {
		$sql_string="UPDATE autos
			SET
			reg_number='".$this->reg_number."',
			type='".$this->type."',
			color='".$this->color."'
			WHERE auto_id=".$this->auto_id."";

		mysqli_query($conn,$sql_string);
	}
	
	function delete($conn) {
		$sql_string ="DELETE FROM autos
			WHERE auto_id=" .$this->auto_id. "";
			
		mysqli_query($conn, $sql_string);
	}
	
	function countAutos($conn) {
		$sql_string = "SELECT * FROM autos";		
		$num_rows = mysqli_num_rows( mysqli_query($conn, $sql_string) );
		return $num_rows;
	}
}
?>