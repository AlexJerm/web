<?php 
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$auto = new Auto();

if (isset($_SESSION['login']) && $_SESSION['login'] == 1)
{

	$auto->model_id = $_POST['model'];
	$auto->reg_number = $_POST['reg_number'];
	$auto->color = $_POST['color'];
	$auto->type = $_POST['type'];
	$auto->add($conn);
	
}

header("Location: index.php");
die();
?>