<?php 
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$model = new Model();

if (isset($_SESSION['login']) && $_SESSION['login'] == 1)
{
	$model->model_name = $_POST['model_name'];
	$model->mark_id = $_POST['mark_id'];
	$model->add($conn);
	
}

header("Location: index.php");
die();
?>