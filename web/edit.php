<?php
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$auto = new Auto();
$auto->getById($conn, $_GET['id']);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Edit Car with ID = <?php echo $_GET['id']; ?></title>
		<meta charset='utf-8'>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	</head>
	
	<body>
		<div class="container"> 
			<div class="panel panel-success">
				<div class="panel-heading"><h4>EDIT CAR</h4></div>
				<div class="panel-body">
						
					<form action="update_car.php" method ="post">
					
						<input type="hidden" name="auto_id" value="<?php echo  $auto->auto_id; ?>">
						<div>
						<label>Reg. Number:</label>
						<input type="text" name="reg_number" value="<?php echo $auto->reg_number; ?>">
						</div>
						
						<div>
						<label>Body Type:</label>
						<select name="type" id="type">
							<option value="coupe" <?php if( $auto->type == "coupe") echo "selected";?>>Купе</option>
							<option value="sedan" <?php if( $auto->type == "sedan") echo "selected";?>>Седан</option>
							<option value="hatchback" <?php if( $auto->type == "hatchback") echo "selected";?>>Хэтчбэк</option>
							<option value="van" <?php if( $auto->type == "van") echo "selected";?>>Ван</option>
							<option value="minivan" <?php if( $auto->type == "minivan") echo "selected";?>>Миниван</option>
						</select>
						</div>
						
						<div>
						<label>Color:</label>
						<select name="color" id="color">
									<option value="black"<?php if ($auto->color == "black") echo "selected"; ?>>Чёрный</option>
									<option value="white"<?php if ($auto->color == "white") echo "selected"; ?>>Белый</option>
									<option value="red"<?php if ($auto->color == "red") echo "selected"; ?>>Красный</option>
									<option value="blue"<?php if ($auto->color == "blue") echo "selected"; ?>>Синий</option>
						</select>
						</div>
						
						<div>
							<input type="submit" value="Save changes">
						</div>
					
					</form>
				</div>
			</div>
		</div>
	
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	</body>
</html>