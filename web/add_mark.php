<?php 
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$mark = new Mark();

if (isset($_SESSION['login']) && $_SESSION['login'] == 1)
{
	$mark->mark_name = $_POST['mark_name'];
	$mark->add($conn);	
}

header("Location: index.php");
die();
?>