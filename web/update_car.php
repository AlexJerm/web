<?php
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$auto = new Auto();
$auto->auto_id = $_POST['auto_id'];
$auto->reg_number = $_POST['reg_number'];
$auto->color = $_POST['color'];
$auto->type = $_POST['type'];

$auto->update($conn);

header("Location:index.php");
die();
?>