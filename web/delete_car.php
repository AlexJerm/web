<?php
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$auto = new Auto();
$auto->auto_id = $_GET['auto_id'];
$auto->delete($conn);

header("Location: index.php");
die();
?>