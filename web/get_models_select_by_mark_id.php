<?php
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$mark = new Mark();
$mark->mark_id = $_GET['mark_id'];

$model = new Model();
echo $model->getModelsByMarkId($conn, $mark->mark_id);
?>
