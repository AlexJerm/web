<?php
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$mark = new Mark();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Admin Console.</title>
		<meta charset='utf-8'>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="../admin/css/main.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>	
		<script src="script/collapse.js"></script>	
	</head>
	
	<body>		
		<div class="container">

		  <!-- Static navbar -->
		  <nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
			  <div class="navbar-header">
					<img src="http://www.bilisimdergi.com/upload/Image/haziran2010/3-1-anasayfa.jpg" width="100" height="50" alt="" />
					Auto Catalogue
			  </div>
			  
			  <div id="navbar" class="navbar-collapse collapse">
			  
				<ul class="nav navbar-nav">
				</ul>
				
				<ul class="nav navbar-nav navbar-right">
					<?php
						if (isset($_SESSION['login']) && $_SESSION['login'] == 1)
						{
					?>
							<li><a>Добро пожаловать, <?php echo $_SESSION['real_name']; ?></a></li>
							<li><a href="logout.php"><span class="glyphicon glyphicon-off"></span></a></li>						
					<?php
						}
					?>
				</ul>
			  </div><!--/.nav-collapse -->
			</div><!--/.container-fluid -->
		  </nav>

		</div> <!-- /container -->
			
			<div class="container container-body">					
				<div class="row">
				
					<div class="col-md-3 col-nav">
						<div class="inner-div site-nav">
							<?php require_once("nav.php"); ?>
						</div>
					</div>
					<div class="col-md-9 col-content">
						<div class="inner-div content">
							<table id="table_autos" class="table table-hover"></table>
						</div>
						
					</div>
				</div>
			</div>

			<script type="text/javascript">
			$("#mark_select").change(function() {
				var selected_mark_id = $("#mark_select").val();
				var url_string = 'get_models_select_by_mark_id.php?mark_id=' + selected_mark_id;
				$('#model_select').load(url_string);
				$('#model_select').prop('disabled', false);
			});
			
			$("#mark_select_add").change(function() {
				var selected_mark_id = $("#mark_select_add").val();
				var url_string = 'get_models_select_by_mark_id.php?mark_id=' + selected_mark_id;
				$('#model_select_add').load(url_string);
				$('#model_select_add').prop('disabled', false);
			});
			
			$("#model_select").change(function() {
				var selected_mark_id = $("#mark_select").val();
				var selected_model_id = $("#model_select").val();
				$.ajax({
					url: 'get_autos.php',
					data: "model_id=" + selected_model_id,
					success: function(data)
					{
						$("#table_autos").html(data);
					}
				
				});
			});

			$(document).ready(function(){
				$("#show_all").click(function(){
					$.ajax({
					url: 'get_autos.php',
					data: "show_all=1",
					success: function(data)
					{
						$("#table_autos").html(data);
					}
				
					});
				});	

				$('.collapse').collapse();
			});
			</script>
	</body>
</html>