<?php
session_start();
function __autoload($class_name) {
	include 'classes/' .$class_name. '.php';
}

$connection = new Connect();
$conn = $connection->get();

$auto = new Auto();

if(isset($_GET['show_all']) && $_GET['show_all'] == 1)
{					
	$getAutosResult = $auto->getAllAutos($conn);
}
else 
{
	$getAutosResult = $auto->get($conn, $_GET['model_id']);
}				
?>
<thead>
		<th>EDIT</th>
		<th>DELETE</th>
		<th>REG. NUMBER</th>
		<th>MARK</th>		
		<th>MODEL NAME</th>
		<th>COLOR</th>
		<th>TYPE</th>
</thead>
<tbody>
<?php
while ($getAutosRow = mysqli_fetch_array($getAutosResult))
{
?>
	<tr>
		<td>
			<a href="edit.php?id=<?php echo $getAutosRow['auto_id']; ?>" target="_blank">
				<span class="glyphicon glyphicon-wrench"></span>
			</a>
		</td>
		<td>
			<a href="delete_car.php?auto_id=<?php echo $getAutosRow['auto_id']; ?>">
				<span class="glyphicon glyphicon-remove"></span>
			</a>
		</td>
		<td><?php echo $getAutosRow['reg_number']; ?></td>
		<td><?php echo $getAutosRow['mark_name']; ?></td>
		<td><?php echo $getAutosRow['model_name']; ?></td>
		<td><div style="margin-left:auto;margin-right:auto;width: 15px;height: 15px;background:<?php echo $getAutosRow['color']?>" ></div></td>
		<td><?php echo $getAutosRow['type']; ?></td>
	</tr>
<?php
}
?>
</tbody>