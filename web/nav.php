			<?php
			if (isset($_SESSION['login']) && $_SESSION['login'] == 1)
			{
			?>
				
				<select class="form-control input-sm" name="mark" id="mark_select">
					<option value="0" selected="selected">Select mark</option>
					
					<?php echo $mark->getAllMarks($conn); ?> 
					
				</select>
				
				<select class="form-control input-sm" name="model" id="model_select" disabled="disabled"></select>
				<button id="show_all" class="btn btn-default" >Show All</button>
				
				<br>
				<br>
				
				
				<div class="panel-group" id="accordion"> <!-- Accordion begining -->
				
				 <div class="panel panel-info">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#new_auto">Add new Auto</a>
					  </h4>
					</div>
					<div id="new_auto" class="panel-collapse collapse in">
					  <div class="panel-body">
					  
						<form action="add_car.php" method="POST">
							<div class="form-group">
								<label>Mark</label>
								<select id="mark_select_add" class="form-control input-sm">
								<option value="0" selected="selected">Select mark</option>
								
								<?php echo $mark->getAllMarks($conn); ?>
								</select>
								
								<label>Model</label>
								<select class="form-control input-sm" name="model" id="model_select_add" disabled="disabled"></select>
								
								<label>Reg. Number:</label>
								<input class="form-control input-sm" type="text" name="reg_number" value="">
								
								<label>Body Type:</label>
								<select class="form-control input-sm" name="type" id="type">
									<option value="coupe" selected>Купе</option>
									<option value="sedan" >Седан</option>
									<option value="hatchback">Хэтчбэк</option>
									<option value="van">Ван</option>
									<option value="minivan">Миниван</option>
								</select>
								
								<label>Color:</label>
								<select class="form-control input-sm" name="color" id="color">
									<option value="black">Чёрный</option>
									<option value="white" >Белый</option>
									<option value="red" >Красный</option>
									<option value="blue">Синий</option>
								</select>
								<br>
								<button type="submit" class="btn btn-default" >Добавить</button>
							</div>
						</form>
					  </div>
					</div>
				  </div>
				  
				  <div class="panel panel-info nospacing">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#new_mark">Add New Mark</a>
					  </h4>
					</div>
					<div id="new_mark" class="panel-collapse collapse in">
					  <div class="panel-body">
						<form action="add_mark.php" method="POST">
							<div class="form_group">
								<label>Mark Name:</label>
								<input class="form-control input-sm" type="text" name="mark_name" value="">
								<br>
								<button type="submit" class="btn btn-default" >Добавить</button>
							</div>
						</form>
					  </div>
					</div>
				  </div>
				  
				    <div class="panel panel-info nospacing">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#new_model">Add New Model</a>
					  </h4>
					</div>
					<div id="new_model" class="panel-collapse collapse in">
					  <div class="panel-body">
						<form action="add_model.php" method="POST">
							<div class="form-group">
								<label>Mark</label>
								<select name="mark_id" id="mark_select_add" class="form-control input-sm">
								<option value="0" selected="selected">Select mark</option>
								
								<?php echo $mark->getAllMarks($conn); ?>
								
								</select>	

								<label>Model Name:</label>
								<input class="form-control input-sm" type="text" name="model_name" value="">
								<br>
								<button type="submit" class="btn btn-default" >Добавить</button>
							</div>
						</form>
					  </div>
					</div>
				  </div>
				
				</div> <!-- Accordion ending -->
			<?php
			}
			else
			{
			?>
						<form action="login.php" method="post">
						<div class="input-group">
							<div class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></div>
							<input class="form-control" name="username" type="text" placeholder="Username">
						</div>	
						
						<div class="input-group">
							<div class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></div>
							<input class="form-control" name="password" type="password" placeholder="Password">
						</div>

						<div>
							<button type="submit" class="btn btn-default" >Sign in</button>
						</div>
			<?php
				if(isset($_SESSION['error_msg']) && $_SESSION['error_msg'] != null)
				{
			?>
					<div class="error_field">
						<label><?php echo $_SESSION['error_msg']; ?></label>
					</div>
			<?php
				}
			}			
			?>	
						</form>