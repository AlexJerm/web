<?php session_start() ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Test Work</title>
		<meta charset='utf-8'>
		<!-- BOOTSTRAP: Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		
		<!-- BOOTSTRAP: Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
		
		<!-- CSS AND JQUERY-->
		<script src="js/jquery-2.1.1.min.js"></script>
		<link rel="stylesheet" href="css/main.css">
	</head>
	
	<body>
		<div class="container"> 
			<div class="panel panel-success">
				<div class="panel-heading"><h4>TEST WORK</h4></div>
				<div class="panel-body">
						
						<?php 
							// LET THE CLIENT KNOW DATA WAS LOADED TO DB
							if (isset($_SESSION['msg']) && $_SESSION['msg'] != 0)
							{
								echo "<p> Data was loaded succesfully to DB! Thank you!</p>";
								$_SESSION['msg'] = 0;
							}
							
						?>
						
						<form action="savedata.php">
							<input type="submit" value="Save Data to DB">
						</form>
						
						<br />
						
						<button type="button" id="loadButton">Load Data from DB</button>
						
						<br />
						<br />
						
						<select id="sel">
						</select>
				</div>
			</div>
		</div>
	
		<!-- BOOTSTRAP: Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		
		<script type="text/javascript">
		$ ( document ).ready(function() {
		
			$("#loadButton").click(function() {
				$.getJSON('getdata.php', function(data) {
					// VARS TO STORE COMPARABLE DATA
					var tempLinn = '';  					
					var tempAdr = '';
					
					for (i = 0; i < data.length; i++)
					{	
						// IF WE HAVE SAME TOWN BUT DIFFERENT ADDRESS
						// WE ADD ADDRESS TO THE CORRESPONDING TOWN
						if(tempLinn == data[i].Linn && tempAdr != data[i].Address)
						{
							$('#sel').append('<option value="' +data[i].id+ '">' +data[i].Address+'</option>');
							tempAdr = data[i].Address;
							continue;
						}
						
						// IF TOWN DOES NOT MATCH PREVIOUS TOWN ANYMORE
						// WE CLOSE PREVIOUS <optgroup> ELEMENT AND
						// OPEN UP A NEW ONE						
						if(tempLinn != data[i].Linn)
						{
							$('#sel').append('</optgroup>');
							$('#sel').append('<optgroup label=' +data[i].Linn+ '>');
							$('#sel').append('<option value="' +data[i].id+ '">' +data[i].Address+'</option>');
							tempLinn = data[i].Linn;
						}
					}
					
				});
			});
			
		});
		</script>
	</body>
</html>
