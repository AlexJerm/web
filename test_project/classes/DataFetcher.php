<?php
class DataFetcher {

	function fetchData($conn) {
		
		$sqlString = "SELECT id, Linn, Aadress
							FROM pakiautomaadid
							ORDER BY Linn";

		$dataResult = mysqli_query($conn, $sqlString);

		$data = array();
		
		// SELECT ONLY THE DATA WE ARE INTERESTED IN
		// Linn and Address
		while ($getDataRow = mysqli_fetch_array($dataResult))
		{
			// ENCODE DATA TO UTF-8 FORMAT
			$data[] = array(
								"id" => $getDataRow['id'],"Linn" => utf8_encode($getDataRow['Linn']), 
								'Address' => utf8_encode($getDataRow['Aadress'])
								
							);
	
		}

		return json_encode($data);
	}
}
?>