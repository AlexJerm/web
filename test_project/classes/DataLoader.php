<?php
class DataLoader {

	private $data;
	
	function __construct(){
		$fileName = file('https://www.omniva.ee/public/files/failid/EST_pakiautomaatide_nimekiri.csv');
		$temp = array_map(array($this, 'stringToArray'), $fileName);
		
		$this->data = array();
		
		foreach($temp as $row)
			$this->data[] = explode(";", $row);
	}
	
	// SAVE DATA TO DB
	function saveData($myConnect) {
	
		$name;
		$linn;
		$aadress;
		$sihtnumber;
		$sqlString;
		
		foreach($this->data as $row)
		{
			if( $row[0] != '' && $row[1] != '' && $row[0] != 'Pakiautomaadid Eestis'){				
				$name = $row[0];
				$linn = $row[1];
				$aadress = $row[2];
				$sihtnumber = $row[3];
				
				$sqlString = "INSERT INTO 
								pakiautomaadid (ID, Name, Linn, Aadress, Sihtnumber)
								VALUES (NULL, '".$name."', '".$linn."', '".$aadress."', '".$sihtnumber."');
							 ";
					 
				mysqli_query($myConnect, $sqlString);
			}
		}
	}
	
	function deleteOldData($myConnect) {
	
		$sqlString = "TRUNCATE TABLE pakiautomaadid";
		mysqli_query($myConnect, $sqlString);
	}

	function stringToArray($str) {
		return $str;
	}
	
	function getData() {
		return $this->data;
	}
	
	function printDump() {
		echo "<pre>";
		var_dump($data);
		echo "<pre>";
	}
}
?>