<?php
require_once('tools.php');

$dataLoader = new DataLoader();
$data = $dataLoader->getData();

// DELETE THE OLD DATA AND UPDATE
$dataLoader->deleteOldData($conn);
$dataLoader->saveData($conn);

// INFORMATION FOR A CLIENT
$_SESSION['msg'] = 1;

header("Location: index.php");
die();
?>